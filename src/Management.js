import React from 'react';
import { Alert, Panel, Tabs, Tab, Button, Modal } from 'react-bootstrap';
	
const requests = [
	{
		name: "Test program",
		desc: "Program Description",
		explain: "Program Explanation",
		permissions: ["permisson1", "permisson2", "permission3", "..."],
		platform: "NodeJs",
		programFile: "code.zip"
	},
	{
		name: "Food Company",
		desc: "Locating popular areas around Singapore",
		explain: "Program aims to show all the locations in Singapore which have a promising number of customers which are integrated into google maps. Parts of the mapthat light up red on popular areas.",
		permissions: ["Access to food truck's routes", "Access to food truck's operating hours","Access to manpower allocation"],
		platform: "NodeJs",
		programFile: "code.zip"
	},
	{
		name: "Finance Company",
		desc: "Calculate the profit loss of the company",
		explain: "Program aims to calculate the profit loss of a company using the expenditure and revenue information of the company.",
		permissions: ["Access to expenditure records of company", "Access to revenue records of company"],
		platform: "Python",
		programFile: "code.zip"
	}
	]

function MyModal({close, requests}) {
	return (
		<Modal.Dialog>
			<Modal.Header>
				<Modal.Title>{requests.desc}</Modal.Title>
			</Modal.Header>

			<Modal.Body><b>Program Description:</b> <br/> {requests.explain} <br/><br/> <b>Permissions:</b> <br/> {requests.permissions.map((request) => <p>- {request}</p>)} <br/> <b>Platform:</b> {requests.platform} <br/><br/> <b>Program File:</b> {requests.programFile}</Modal.Body>

			<Modal.Footer>
				<Button bsStyle="primary" onClick={close}>Close</Button>
			</Modal.Footer>
		</Modal.Dialog>
	)
}
	
class Management extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false,
			modalInfo: null
		};
		
		this.view = this.view.bind(this);
		this.close = this.close.bind(this);
	}
	
	view(index) {
		this.setState({
			showModal: true,
			modalInfo: requests[index],
		});
	}
	
	close() {
		this.setState({
			showModal: false,
		});
	}
	
	render() {
		return (
	<div>
		{this.state.showModal && <MyModal close={this.close} requests={this.state.modalInfo}/>}
		<Tabs defaultActiveKey={2} id="tab1">
			<Tab eventKey={1} title="Running Instances">
				Tab 1 content
			</Tab>
			<Tab eventKey={2} title="Open Requests">
				<br/>
		
				{requests.map((request, index) => (
				
				
				<Panel bsStyle="info">
					<Panel.Heading>{request.name}</Panel.Heading>
					<Panel.Body>{request.desc}</Panel.Body>
					<Panel.Footer><Button onClick={() => this.view(index)} bsStyle="success" bsSize="small">View</Button></Panel.Footer>				
			   </Panel>
				))}
	
			</Tab>
		</Tabs>
	</div>
	
);
	}
}

export default Management;
