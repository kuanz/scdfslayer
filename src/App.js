import React from 'react'
import { Navbar, NavItem, Nav, NavDropdown, MenuItem } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import Management from './Management.js';
import Submission from './Submission.js';
import Home from './Home.js';

const Main = () => (
  <Router>
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">
              SCDFSLAYER
            </Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="#">
            <Link to="/management">
              Management
            </Link>
          </NavItem>
          <NavItem eventKey={2} href="#">
            <Link to="/submission">
              Create application
            </Link>
          </NavItem>
        </Nav>
      </Navbar>
      <div>
        <Route exact path="/" component={Home}/>
        <Route exact path="/management" component={Management}/>
        <Route exact path="/submission" component={Submission}/>
      </div>
    </div>
  </Router>
)
export default Main

